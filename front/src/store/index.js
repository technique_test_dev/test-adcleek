import { createStore } from 'vuex'
import api from './api'

export default createStore({
  state: {
    weathers:[],
    cities:[]
  },
  mutations: {
    setCities: (state, cities) =>{
      state.cities = cities
    },
    setWeather: (state, weathers) =>{
      state.weathers = weathers
    },
  },
  actions: {
    async cities({commit}){
      let cities = await (await api().get("city")).data
      commit('setCities', cities)
    },
    async weather({commit},city){
      let weathers = await (await api().get("city/"+city)).data
      
      commit('setWeather', weathers)
    }
  },
  getters:{
    weathers: (state) => state.weathers,
    cities: (state) => state.cities,
  }
})
