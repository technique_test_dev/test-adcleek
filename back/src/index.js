if(process.env.NODE_ENV != 'production'){
  require('dotenv').config();
}

const db = require('./database');
// pour permettre la communication entre le front et le back en dev.
const cors = require('cors');
const express =  require('express')

const app = express()
const port = process.env.PORT || 3000
const axios = require('axios');

const TOKEN = process.env.WEATHER_API_TOKEN
var weatherUrl = "https://api.meteo-concept.com/api/forecast/daily?token="+TOKEN;
var key = '&insee=';

weatherUrl +=key

// a ajouter pour la communication entre le front et le back en dev lorsque express est instancié
app.use(cors({
  credentials: true
}));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Initialisation de la base avec les deux tables nécessaires (à garder)
db.init();
// exemple de requete sql à supprimer
db.all('select * from city').then((rows) => {
  console.table(rows);
});

// dans le cas où le front est fait en js natif, voici une ligne de commande à ajouter pour servir le front à partir du projet node
// si vous faîtes du VueJS ou du React ce n'est pas nécessaire
// dans ce cas il n'est pas nécessaire d'utiliser la partie cors (ligne 6 à 8)
//app.use('/', express.static('../../front/'));


function getWeatherDataForOneDay (city){
  return axios
    .get(weatherUrl+city)
    .then(rs => rs.data)
    .catch((error) =>{ console.log(error); });
}

async function getAllCity(){
  return await db.all(`select * from city`)
}
async function getCity(city){
  return await db.get(`select * from city where id="${city}"`)
}
async function getForecast(insee){
  return await db.all(`select * from forecast where insee="${insee}"`)
}
async function insertDbForecast(data){
  console.log("inserted")
  console.log(data)
  db.run(`insert into forecast('date', 'insee','details') values('${data.datetime}','${data.insee}','${JSON.stringify(data)}')`)
}

app.listen(port , ()=>console.log(`Server running on port ${port}`));

app.get('/api/city', async (req, res) => {
  res.status(200).send(await getAllCity())
})

app.get('/api/city/:ville', async (req, res) => {

  db
  .all(`select * from city where id=${req.params.ville}`)
  .then(rows => rows[0] )
  .then(async rs => {

    let forcast = await getForecast(rs.insee)

    if(!forcast || forcast.length<1){
      console.log("not exist")
      let previsions = await getWeatherDataForOneDay(rs.insee)

      previsions.forecast.forEach(async prevision=> insertDbForecast(prevision))

    }

    rs =  await getForecast(rs.insee)

    return rs
 
  })
  .then(row => res.status(200).send(row))
})